package info.renjithv.jira.plugins.sysadmin.jira.webwork;

import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebSudoRequired
public class LiveViewerModuleAction extends JiraWebActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(LiveViewerModuleAction.class);
    private String filename;

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        return super.doExecute();
    }

    @Override
    @RequiresXsrfCheck
    public String execute() throws Exception {

        return super.execute(); //returns SUCCESS
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
