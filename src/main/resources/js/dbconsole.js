var editor;
var updateDisplay = function(data){
    if(data.error){
        AJS.$('#sqlmessage').text('Error in executing SQL - ' + data.error);
        AJS.$('#sqlerror').show();
    }
    //console.log(data);
    var head = AJS.$("#headrow");
    head.empty();
    var rows = AJS.$("#bodyarea");
    rows.empty();
    if(data.resultType == 1){
        data.columns.forEach(function(col){
            head.append("<th>" + col+ "</th>");
        });
        data.rows.forEach(function(row){
            var newrow = AJS.$('<tr></tr>');
            row.forEach(function(val){
                newrow.append("<td>" + val+ "</td>");
            });
            rows.append(newrow);
        });
    }
    else
    {
        head.append("<th>Updated rows: " + data.updatedCount+ "</th>");
    }
};

var handleTableChange = function(event){
    AJS.$('#columndiv').show();
    AJS.$('#columns').empty();
    if(AJS.$('#tables').find(":selected").attr('value') != "ignore"){
        tableMap[AJS.$('#tables').find(":selected").text()].forEach(function(col){
            AJS.$('#columns').append("<option value=\"" + col.name+ "\">"+col.name+"</option>");
        });
        updateSql();
    }
};

var tableMap = {};
var updateTables = function(data){
    data.forEach(function(table){
        tableMap[table.name] = table.columns;
    });
    data.forEach(function(table){
        AJS.$('#tables').append("<option value=\"" +table.name+ "\">" + table.name+ "</option>")
    });

    AJS.$('#tables').change(handleTableChange);

    AJS.$('#columns').change(updateSql);
};

var executeSql = function(event){
    AJS.$('#sqlerror').hide();
    event.preventDefault();
    var sql = editor.getValue();
    if(!sql)
    {
        alert("Enter an SQL statement to execute");
    }
    else{
        AJS.$('#waiticon').show();
        AJS.$.getJSON(AJS.contextPath() + "/rest/dbconsolerest/latest/console/execute?sql=" +
            encodeURIComponent(sql))
            .success(updateDisplay)
            .fail(function() {
                AJS.$('#sqlmessage').text('Error in executing SQL. Try refreshing the page and re-executing the query.');
                AJS.$('#sqlerror').show();
            })
            .always(function() {
                AJS.$('#waiticon').hide();
            });
    }
};

var updateSql = function(){
    var sqlColumns = AJS.$("#columns option:selected").map(function() {
        return "\"" + this.value + "\"";
    }).get().join();
    var sqlTable = AJS.$('#tables').find(":selected").text();
    var finalSql = '';
    if(0 == sqlColumns.length){
        finalSql = "SELECT * FROM \"" + sqlTable + "\"";
    }
    else
    {
        finalSql = "SELECT " + sqlColumns + " FROM \"" + sqlTable + "\"";
    }
    editor.setValue(finalSql);
};

AJS.$('document').ready(function () {
    editor = CodeMirror.fromTextArea(document.getElementById('sql'), {
        mode: 'text/x-sql',
        indentWithTabs: true,
        smartIndent: true,
        lineNumbers: true,
        matchBrackets : true,
        autofocus: true
    });

    AJS.$('#waiticon').hide();
    AJS.$('#waiticongetdata').hide();
    AJS.$('#columndiv').hide();
    AJS.$('#sqlerror').hide();
    AJS.$('#execute-btn').click(executeSql);

    // add ctrl+enter shotcut for query execution
    AJS.$(document).off('keypress', require('jira/util/forms').submitOnCtrlEnter);
    AJS.$(document).keyup(function (event) {
        if (event.which == 13 && event.ctrlKey) {
            executeSql(event);
        }
    });

    AJS.$.getJSON(AJS.contextPath() + "/rest/dbmetarest/latest/meta/data")
        .success(updateTables)
        .fail(function(){
            AJS.$('#sqlmessage').text('Error in retrieving the tables. Try refreshing the page.');
            AJS.$('#sqlerror').show();
        })
        .always(function(){
            AJS.$('#waiticontable').hide();
        });
});