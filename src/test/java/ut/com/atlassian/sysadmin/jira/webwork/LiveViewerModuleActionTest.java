package ut.com.atlassian.sysadmin.jira.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @since 3.5
 */
public class LiveViewerModuleActionTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //LiveViewerModuleAction testClass = new LiveViewerModuleAction();

        throw new Exception("LiveViewerModuleAction has no tests!");

    }

}
